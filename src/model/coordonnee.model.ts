import {
    getModelForClass,
    prop,
    // index,
  } from "@typegoose/typegoose";

  //--------------------------------USER SCHEMA ---------------------------------------------//
  export class Coordonnee {
    @prop({ required: true })
    lon: string;
  
    @prop({ required: true })
    lat: string;

    @prop()
    ts: string;

    @prop()
    aqius: number;
    
    @prop()
    mainus: string;
    
    @prop()
    aqicn: number;
    
    @prop()
    maincn: string;

    @prop()
    date_time: string;
  }
  
  const CoordonneeModel = getModelForClass(Coordonnee);
  
  export default CoordonneeModel;
  