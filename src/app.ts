import * as dotenv from 'dotenv'
dotenv.config()
import express from "express";
import connectDB from './utils/connectDB';
import log from './utils/logger';
import router from './routes'
import {cronJob} from './utils/iqair';
import { createParisAir } from './services/coordonnee.service';

const cron = require("node-cron");

const app = express();
app.use(express.json());

app.use("/api",router)

//----------------- DATABASE CONNECTION ----------------------//
connectDB();

//----------------- CRON JOB ----------------------//
cron.schedule("*/60 * * * * *", async function () {
	const result = await cronJob();
    console.log(result);

	const newCoord = await createParisAir(result);
	newCoord.save()
		.then(item => {
			item;
			console.log("item saved to database");
		})
		.catch(err => {
			console.log("unable to save to database ---> ", err);
		})

	console.log("---------------------");
	console.log("running a task every 1 minute");
  });


//----------------- SERVER RUN ----------------------//
const PORT = process.env.PORT;
const IP_ADDRESS = process.env.IP_ADDRESS;
app.listen(PORT, () => {
	log.info(`App running on ${IP_ADDRESS}:${PORT}`)
});