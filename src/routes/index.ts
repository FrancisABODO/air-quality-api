import express from "express";
import coordonnee from './coordonnee.routes';

const router = express.Router();

router.get('/healthCheck', (_, res) => res.sendStatus(200));
router.use(coordonnee)

export default router