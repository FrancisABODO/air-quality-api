import express from "express";
import { getAirQuality } from "../controller/coordonnee.controller";
const router = express.Router();

router.get("/air", getAirQuality)

export default router;
