import CoordonneeModel, { Coordonnee } from "../model/coordonnee.model"

export const createParisAir = (input:Partial<Coordonnee>) => {
    return CoordonneeModel.create(input)
}

export const findAirByLonLat = (longitude: string, latitude: string) => {
    return CoordonneeModel.findOne({longitude, latitude});
}
