
export const getAirFromIQAIR = async (lon: string, lat: string, api: string | undefined) => {
    const url = process.env.BASE_URL;
    const response = await fetch(`${url}v2/nearest_city?lat=${lat}&lon=${lon}&key=${api}`);
    const jsonResponse = await response.json();

    return jsonResponse;
}

export const cronJob = async () => {
    const url = process.env.BASE_URL;
    const api = process.env.IQAIR_API_KEY;
    const response = await fetch(`${url}v2/nearest_city?lat=48.856613&lon=2.352222&key=${api}`);
    const jsonResponse = await response.json();

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    
    //console.log(dateTime)

    const result = jsonResponse.data.current.pollution;
    result["date_time"]= dateTime;
    result["lon"] = "2.352222";
    result["lat"] = "48.856613";
    //console.log(result);
    
    return result;

}