import mongoose from "mongoose";
import log from './logger';


const connectDB = async () => {
	mongoose.set('strictQuery', true);
    const databaseUrl= process.env.MONGODB_URI
	try {
		await mongoose.connect(databaseUrl as string);
        log.info('Database connected')
	} catch (error) {
        process.exit(1)
	}
};


export default connectDB;