import { object, string, TypeOf } from "zod";

//------------------------------ VERIFY USER SCHEMA -----------------------------//
export const VerifyCoordonneeSchema = object({
    params: object({
      lon: string(),
      lat: string(),
    }),
  });

  export type VerifyCoordonneeInput = TypeOf<typeof VerifyCoordonneeSchema>["params"];
