import { Request, Response } from "express";
import { VerifyCoordonneeInput } from "../schema/coordonnee.schema";
import { getAirFromIQAIR } from "../utils/iqair";

//----------------------------------- FIND AIR QUALITY BY LONGITUDE & LATITUDE ----------------------------------//
export const getAirQuality = async (
    req: Request<VerifyCoordonneeInput>,
    res: Response
  ) => {
    const {lon, lat} = req.body
    const api_key = process.env.IQAIR_API_KEY;

    const result = await getAirFromIQAIR(lon, lat, api_key);

    return res
      .status(200)
      .json({"result": result.data.current.pollution});
  }