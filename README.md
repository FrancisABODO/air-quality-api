# Mini-Projet "AIR QUALITY":
# But:
Création d'un API REST en Node Js.

# Contexte:
Le but de ce projet est de créer un API REST chargé d'exposer les informations sur la qualité de l'air d'une ville la plus proche à des coordonnées GPS
en utilisant iqair ( https://www.iqair.com/fr/commercial/air-quality-monitors/airvisual-platform/api )


# Architecture du Projet

À la racine du projet, nous avons deux dossiers principales à savoir:
- *config*: qui contient les variables pour la mise en production
- *src*: qui contient les éléments qui constituent notre application
  
À l'intérieur de notre dossier *src*, nous avons les dossiers:
1. controller
2. middleware
3. model
4. routes
5. schema
6. services
7. utils

# Tâches réalisées:

1. Création de notre endpoint
````
GET /api/air
BODY: {
    "lat": "35.98",
    "lon": "140.33"
}
Response :
{
    "result": {
        "ts": "2023-06-27T11:00:00.000Z",
        "aqius": 68,
        "mainus": "p2",
        "aqicn": 29,
        "maincn": "p2"
    }
}
`````

2. Création du Cron

Le Cron vérifie la qualité de l'air de Paris et s'exécute toutes les minutes. le resultat est stocké dans mongodb suivant ce format:
````
_id: 649a8f82333c8899d2600240
lon: "2.352222"
lat: "48.856613"
ts: "2023-06-27T07:00:00.000Z"
aqius: 30
mainus: "o3"
aqicn: 23
maincn: "o3"
date_time: "2023-6-27 8:28:2"
__v: 0
````

3. Création du Dockerfile