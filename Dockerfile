FROM node:14.20.0-alpine
WORKDIR /app
COPY config config
COPY src src
COPY package.json tsconfig.json .
RUN npm install
RUN ls -al -R
RUN npm run build
## RUN npm install pm2 -g
EXPOSE 5000
CMD ["npm","start"]